package com.hsone.finest.feescheduler.utils;

import java.time.Instant;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;

public class DateTimeUtils {
  private DateTimeUtils() { }

  /**
   * Takes date in long representation and returns date as string
   * in standard {@code OffsetDateTime} format
   *
   * @param milliseconds date in milliseconds
   * @return date in string representation
   */
  public static String millisecondsToDateTimeString(Long milliseconds) {
    return millisecondsToDateTime(milliseconds).toString();
  }

  private static OffsetDateTime millisecondsToDateTime(Long milliseconds) {
    Instant timestamp = Instant.ofEpochMilli(milliseconds);
    return OffsetDateTime.ofInstant(timestamp, ZoneOffset.UTC);
  }

  /**
   * Takes date in String format in standard {@code OffsetDateTime}
   * and returns its long representation
   * @param dateTime date in string representation
   * @return date in milliseconds
   */
  public static Long stringDateTimeToMilliseconds(String dateTime) {
    return dateTimeToMilliseconds(OffsetDateTime.parse(dateTime));
  }

  private static Long dateTimeToMilliseconds(OffsetDateTime dateTime) {
    return dateTime.toInstant().toEpochMilli();
  }

  /**
     * @return current date in string representation
   */
  public static String current() {
    return millisecondsToDateTimeString(System.currentTimeMillis());
  }
}
