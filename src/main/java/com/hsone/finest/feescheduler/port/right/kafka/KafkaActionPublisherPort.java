package com.hsone.finest.feescheduler.port.right.kafka;

import com.hsone.finest.feescheduler.core.model.kafka.command.Action;
import com.hsone.finest.feescheduler.core.model.kafka.command.AddActionProcessingCommand;
public interface       KafkaActionPublisherPort {

  boolean publishAddActionCommand(AddActionProcessingCommand command);

  boolean publishAddActionEvent(Action command);
}
