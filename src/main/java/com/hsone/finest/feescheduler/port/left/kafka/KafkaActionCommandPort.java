package com.hsone.finest.feescheduler.port.left.kafka;

import com.hsone.finest.feescheduler.core.model.kafka.command.CommandProcessingResult;
import com.hsone.finest.feescheduler.core.model.kafka.command.AddActionProcessingCommand;

public interface KafkaActionCommandPort {

  CommandProcessingResult addAction(AddActionProcessingCommand command);
}
