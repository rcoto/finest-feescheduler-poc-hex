package com.hsone.finest.feescheduler.core.model;

public class ProcedureCategory {
    private Integer procedureCategoryId;
    private String name;
    private String description;

    public Integer getProcedureCategoryId() {
        return procedureCategoryId;
    }

    public void setProcedureCategoryId(Integer procedureCategoryId) {
        this.procedureCategoryId = procedureCategoryId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
