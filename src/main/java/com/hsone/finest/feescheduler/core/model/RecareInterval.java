package com.hsone.finest.feescheduler.core.model;

public class RecareInterval {
    private Integer recareIntervalId;
    private Integer intervalYear;
    private Integer intervalMonth;
    private Integer intervalWeek;

    public Integer getRecareIntervalId() {
        return recareIntervalId;
    }

    public void setRecareIntervalId(Integer recareIntervalId) {
        this.recareIntervalId = recareIntervalId;
    }

    public Integer getIntervalYear() {
        return intervalYear;
    }

    public void setIntervalYear(Integer intervalYear) {
        this.intervalYear = intervalYear;
    }

    public Integer getIntervalMonth() {
        return intervalMonth;
    }

    public void setIntervalMonth(Integer intervalMonth) {
        this.intervalMonth = intervalMonth;
    }

    public Integer getIntervalWeek() {
        return intervalWeek;
    }

    public void setIntervalWeek(Integer intervalWeek) {
        this.intervalWeek = intervalWeek;
    }
}
