package com.hsone.finest.feescheduler.core.model;

public class FeeScheduleItem {
    private Integer feeScheduleItemId;
    private Integer organizationProcedureId;
    private Float fee;

    public Integer getFeeScheduleItemId() {
        return feeScheduleItemId;
    }

    public void setFeeScheduleItemId(Integer feeScheduleItemId) {
        this.feeScheduleItemId = feeScheduleItemId;
    }

    public Integer getOrganizationProcedureId() {
        return organizationProcedureId;
    }

    public void setOrganizationProcedureId(Integer organizationProcedureId) {
        this.organizationProcedureId = organizationProcedureId;
    }

    public Float getFee() {
        return fee;
    }

    public void setFee(Float fee) {
        this.fee = fee;
    }
}
