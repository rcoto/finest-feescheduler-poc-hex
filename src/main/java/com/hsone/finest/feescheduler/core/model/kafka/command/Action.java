package com.hsone.finest.feescheduler.core.model.kafka.command;

import java.util.UUID;
import lombok.Builder;
import lombok.Data;
import lombok.ToString;

@Data
@Builder
@ToString
public class Action {
  private UUID uuid;
  private String name;
  private String description;
  private String subject;
  private String resource;
  private String createdDate;
  private String createdBy;
}
