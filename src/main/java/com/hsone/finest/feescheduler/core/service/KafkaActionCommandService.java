package com.hsone.finest.feescheduler.core.service;

import com.hsone.finest.feescheduler.core.model.kafka.command.CommandProcessingResult;
import com.hsone.finest.feescheduler.core.model.kafka.command.AddActionProcessingCommand;
import com.hsone.finest.feescheduler.port.left.kafka.KafkaActionCommandPort;
import com.hsone.finest.feescheduler.port.right.kafka.KafkaActionPublisherPort;
import org.springframework.stereotype.Service;

@Service
public class KafkaActionCommandService implements KafkaActionCommandPort {

  private final KafkaActionPublisherPort actionCommandPublisher;

  public KafkaActionCommandService(KafkaActionPublisherPort actionCommandPublisher) {
    this.actionCommandPublisher = actionCommandPublisher;
  }

  @Override
  public CommandProcessingResult addAction(AddActionProcessingCommand command) {
    return actionCommandPublisher.publishAddActionCommand(command) ? CommandProcessingResult.SUCCESS : CommandProcessingResult.FAILURE;
  }
}
