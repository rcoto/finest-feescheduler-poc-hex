package com.hsone.finest.feescheduler.core.model.kafka.command;

public enum CommandProcessingResult {
  SUCCESS, FAILURE
}
