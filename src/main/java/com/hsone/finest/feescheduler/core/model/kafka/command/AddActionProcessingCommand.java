package com.hsone.finest.feescheduler.core.model.kafka.command;

import lombok.Builder;
import lombok.Data;
import lombok.ToString;

@Data
@Builder
@ToString
public class AddActionProcessingCommand {
  private Action action;
}
