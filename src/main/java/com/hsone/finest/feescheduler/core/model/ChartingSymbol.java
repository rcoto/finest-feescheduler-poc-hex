package com.hsone.finest.feescheduler.core.model;

public class ChartingSymbol {
    private Integer chartingSymbolId;
    private String name;

    public Integer getChartingSymbolId() {
        return chartingSymbolId;
    }

    public void setChartingSymbolId(Integer chartingSymbolId) {
        this.chartingSymbolId = chartingSymbolId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
