package com.hsone.finest.feescheduler.core.model;

public class RecareType {
    private Integer RecareTypeId;
    private String type;
    private String description;
    private Boolean isDefault;

    public Integer getRecareTypeId() {
        return RecareTypeId;
    }

    public void setRecareTypeId(Integer recareTypeId) {
        RecareTypeId = recareTypeId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getDefault() {
        return isDefault;
    }

    public void setDefault(Boolean aDefault) {
        isDefault = aDefault;
    }
}
