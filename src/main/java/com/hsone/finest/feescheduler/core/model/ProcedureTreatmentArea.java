package com.hsone.finest.feescheduler.core.model;

public class ProcedureTreatmentArea {
    private Integer procedureTreatmentAreaId;
    private String name;
    private String description;

    public Integer getProcedureTreatmentAreaId() {
        return procedureTreatmentAreaId;
    }

    public void setProcedureTreatmentAreaId(Integer procedureTreatmentAreaId) {
        this.procedureTreatmentAreaId = procedureTreatmentAreaId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
