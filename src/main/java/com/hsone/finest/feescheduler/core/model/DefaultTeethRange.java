package com.hsone.finest.feescheduler.core.model;

public class DefaultTeethRange {
    private Integer defaultTeethRangeId;
    private String code;
    private Integer firstToothInRange;
    private Integer lastToothInRange;

    public Integer getDefaultTeethRangeId() {
        return defaultTeethRangeId;
    }

    public void setDefaultTeethRangeId(Integer defaultTeethRangeId) {
        this.defaultTeethRangeId = defaultTeethRangeId;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Integer getFirstToothInRange() {
        return firstToothInRange;
    }

    public void setFirstToothInRange(Integer firstToothInRange) {
        this.firstToothInRange = firstToothInRange;
    }

    public Integer getLastToothInRange() {
        return lastToothInRange;
    }

    public void setLastToothInRange(Integer lastToothInRange) {
        this.lastToothInRange = lastToothInRange;
    }
}
