package com.hsone.finest.feescheduler.adapter.rest.api.kafka;

import com.hsone.finest.feescheduler.adapter.rest.api.kafka.model.ActionRequest;
import com.hsone.finest.feescheduler.core.model.FeeSchedule;
import com.hsone.finest.feescheduler.core.model.kafka.command.Action;
import com.hsone.finest.feescheduler.core.model.kafka.command.AddActionProcessingCommand;
import com.hsone.finest.feescheduler.port.left.kafka.KafkaActionCommandPort;
import com.hsone.finest.feescheduler.utils.DateTimeUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.UUID;

@RestController
public class ActionController {

  private final KafkaActionCommandPort actionCommandHandler;

  @Autowired
  public ActionController(KafkaActionCommandPort actionCommandHandler) {
    this.actionCommandHandler = actionCommandHandler;
  }

  @GetMapping(value = "/kafka")
  public String getFeeScheduleRecords() {
    var msg = "Hello Friend (feeSchedule).";
    System.out.println(msg);
    return msg;
  }

  @PostMapping(path = "/actions", consumes = "application/json")
  public ResponseEntity<?> createActions(@RequestBody ActionRequest request) {

    var actionProcessingCommand = AddActionProcessingCommand
        .builder()
        .action(Action.builder()
            .uuid(UUID.randomUUID())
            .name(request.getName())
            .description(request.getDescription())
            .subject(request.getSubject())
            .resource(request.getResource())
            .createdDate(DateTimeUtils.current())
            .createdBy(request.getCreatedBy())
            .build())
        .build();

    actionCommandHandler.addAction(actionProcessingCommand);
    return ResponseEntity.accepted().build();
  }
}
