package com.hsone.finest.feescheduler.adapter.repository;

import com.hsone.finest.feescheduler.adapter.entity.CardSwipeDeviceEntity;
import org.springframework.data.repository.CrudRepository;

public interface CardSwipeDeviceCrudRepository extends CrudRepository<CardSwipeDeviceEntity, Long> {
}
