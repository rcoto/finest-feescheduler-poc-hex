package com.hsone.finest.feescheduler.adapter.kafka;

import com.hsone.finest.feescheduler.avro.action.AddActionCommandSchema;
import com.hsone.finest.feescheduler.core.model.kafka.command.Action;
import com.hsone.finest.feescheduler.core.model.kafka.command.AddActionProcessingCommand;
import com.hsone.finest.feescheduler.port.right.kafka.KafkaActionPublisherPort;

import java.util.concurrent.TimeUnit;

import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Service;
import org.springframework.util.concurrent.ListenableFuture;

@Slf4j
@Service
public class KafkaActionPublisher implements KafkaActionPublisherPort {

  private final String addActionProcessingCommandTopicName;
  private final KafkaTemplate<String, AddActionCommandSchema> addActionCommandProducer;

  public KafkaActionPublisher(@Value("${kafka.topics.testTopic}") String addActionProcessingCommandTopicName,
                             KafkaTemplate<String, AddActionCommandSchema> addActionCommandProducer) {
    this.addActionProcessingCommandTopicName = addActionProcessingCommandTopicName;
    this.addActionCommandProducer = addActionCommandProducer;
  }


  @Override
  public boolean publishAddActionCommand(AddActionProcessingCommand command) {
    try {
      var action = command.getAction();
      var schema = ActionConverter.toAddActionCommandSchema(action);

      ProducerRecord<String, AddActionCommandSchema> record = new ProducerRecord<>(
          addActionProcessingCommandTopicName,
          "ActionProcessingCommandPublisher",
          schema);
      ListenableFuture<SendResult<String, AddActionCommandSchema>> listenableFuture = addActionCommandProducer.send(record);
      listenableFuture.get(TimeUnit.SECONDS.toSeconds(2), TimeUnit.SECONDS);
      log.info("Successfully published AddActionProcessingCommand " + command);
      return true;
    } catch (Exception e) {
      log.error("Failed to publish AddActionProcessingCommand", e);
    }
    return false;
  }

  @Override
  public boolean publishAddActionEvent(Action command) {
    log.info("Successfully published AddActionProcessingCommand " + command.toString());
    return true;
  }
}
