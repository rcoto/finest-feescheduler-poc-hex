package com.hsone.finest.feescheduler.adapter.repository;

import com.hsone.finest.feescheduler.adapter.entity.AddressEntity;
import org.springframework.data.repository.CrudRepository;

public interface AddressCrudRepository extends CrudRepository<AddressEntity, Long> {
}
