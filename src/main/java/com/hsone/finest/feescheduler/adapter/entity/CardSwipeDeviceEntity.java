package com.hsone.finest.feescheduler.adapter.entity;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class CardSwipeDeviceEntity {

    @Id
    private Integer cardSwipeDeviceId;
    private String name;

    public CardSwipeDeviceEntity(final Integer cardSwipeDeviceId, final String name) {
        this.cardSwipeDeviceId = cardSwipeDeviceId;
        this.name = name;
    }

    public CardSwipeDeviceEntity() {

    }

    public Integer getCardSwipeDeviceId() {
        return cardSwipeDeviceId;
    }

    public void setCardSwipeDeviceId(Integer cardSwipeDeviceId) {
        this.cardSwipeDeviceId = cardSwipeDeviceId;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }
}
