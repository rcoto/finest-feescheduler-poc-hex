package com.hsone.finest.feescheduler.adapter.kafka;

import com.hsone.finest.feescheduler.avro.action.ActionSchema;
import com.hsone.finest.feescheduler.avro.action.AddActionCommandSchema;
import com.hsone.finest.feescheduler.core.model.kafka.command.Action;
import com.hsone.finest.feescheduler.core.model.kafka.command.AddActionProcessingCommand;

import java.util.UUID;

public class ActionConverter {

  public static AddActionProcessingCommand toAddActionProcessingCommand(AddActionCommandSchema schema) {
    var action = Action.builder()
        .uuid(UUID.fromString(schema.getAction().getId()))
        .name(schema.getAction().getName())
        .description(schema.getAction().getDescription())
        .subject(schema.getAction().getSubject())
        .resource(schema.getAction().getResource())
        .createdDate(schema.getAction().getCreatedDate())
        .createdBy(schema.getAction().getCreatedBy());
    return AddActionProcessingCommand.builder().action(action.build()).build();
  }

  public static AddActionCommandSchema toAddActionCommandSchema(Action action) {
    return AddActionCommandSchema.newBuilder()
            .setAction(ActionSchema.newBuilder()
                    .setId(action.getUuid().toString())
                    .setName(action.getName())
                    .setDescription(action.getDescription())
                    .setSubject(action.getSubject())
                    .setResource(action.getResource())
                    .setCreatedDate(action.getCreatedDate())
                    .setCreatedBy(action.getCreatedBy()).build())
            .build();
  }
}
