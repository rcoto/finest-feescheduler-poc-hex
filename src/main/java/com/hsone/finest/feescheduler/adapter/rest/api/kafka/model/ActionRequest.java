package com.hsone.finest.feescheduler.adapter.rest.api.kafka.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ActionRequest {
  private String name;
  private String description;
  private String subject;
  private String resource;
  private String createdBy;
}
