package com.hsone.finest.feescheduler.adapter.rest.mapper;

import com.hsone.finest.feescheduler.adapter.entity.AddressEntity;
import com.hsone.finest.feescheduler.core.model.Address;
import java.util.ArrayList;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2023-06-16T15:08:36-0600",
    comments = "version: 1.5.4.Final, compiler: IncrementalProcessingEnvironment from gradle-language-java-7.6.1.jar, environment: Java 17.0.6 (Oracle Corporation)"
)
@Component
public class AddressMapperImpl implements AddressMapper {

    @Override
    public Address toAddress(AddressEntity addressEntity) {
        if ( addressEntity == null ) {
            return null;
        }

        String addressId = null;
        String address1 = null;
        String address2 = null;
        String postalCode = null;
        String city = null;
        String state = null;

        if ( addressEntity.getAddressId() != null ) {
            addressId = String.valueOf( addressEntity.getAddressId() );
        }
        address1 = addressEntity.getAddress1();
        address2 = addressEntity.getAddress2();
        postalCode = addressEntity.getPostalCode();
        city = addressEntity.getCity();
        state = addressEntity.getState();

        Address address = new Address( addressId, address1, address2, city, state, postalCode );

        return address;
    }

    @Override
    public Iterable<Address> toAddresses(Iterable<AddressEntity> addressEntity) {
        if ( addressEntity == null ) {
            return null;
        }

        ArrayList<Address> iterable = new ArrayList<Address>();
        for ( AddressEntity addressEntity1 : addressEntity ) {
            iterable.add( toAddress( addressEntity1 ) );
        }

        return iterable;
    }

    @Override
    public AddressEntity toAddressEnity(Address address) {
        if ( address == null ) {
            return null;
        }

        AddressEntity addressEntity = new AddressEntity();

        if ( address.getAddressId() != null ) {
            addressEntity.setAddressId( Long.parseLong( address.getAddressId() ) );
        }
        addressEntity.setAddress1( address.getAddress1() );
        addressEntity.setAddress2( address.getAddress2() );
        addressEntity.setPostalCode( address.getPostalCode() );
        addressEntity.setCity( address.getCity() );
        addressEntity.setState( address.getState() );

        return addressEntity;
    }
}
