/**
 * Autogenerated by Avro
 *
 * DO NOT EDIT DIRECTLY
 */
package com.hsone.finest.feescheduler.avro.action;

import org.apache.avro.generic.GenericArray;
import org.apache.avro.specific.SpecificData;
import org.apache.avro.util.Utf8;
import org.apache.avro.message.BinaryMessageEncoder;
import org.apache.avro.message.BinaryMessageDecoder;
import org.apache.avro.message.SchemaStore;

@org.apache.avro.specific.AvroGenerated
public class ActionSchema extends org.apache.avro.specific.SpecificRecordBase implements org.apache.avro.specific.SpecificRecord {
  private static final long serialVersionUID = -2798154361140247961L;
  public static final org.apache.avro.Schema SCHEMA$ = new org.apache.avro.Schema.Parser().parse("{\"type\":\"record\",\"name\":\"ActionSchema\",\"namespace\":\"com.hsone.finest.feescheduler.avro.action\",\"fields\":[{\"name\":\"id\",\"type\":{\"type\":\"string\",\"avro.java.string\":\"String\"},\"logicalType\":\"uuid\"},{\"name\":\"name\",\"type\":{\"type\":\"string\",\"avro.java.string\":\"String\"}},{\"name\":\"description\",\"type\":{\"type\":\"string\",\"avro.java.string\":\"String\"}},{\"name\":\"subject\",\"type\":{\"type\":\"string\",\"avro.java.string\":\"String\"}},{\"name\":\"resource\",\"type\":{\"type\":\"string\",\"avro.java.string\":\"String\"}},{\"name\":\"createdDate\",\"type\":{\"type\":\"string\",\"avro.java.string\":\"String\"}},{\"name\":\"createdBy\",\"type\":{\"type\":\"string\",\"avro.java.string\":\"String\"}}]}");
  public static org.apache.avro.Schema getClassSchema() { return SCHEMA$; }

  private static SpecificData MODEL$ = new SpecificData();

  private static final BinaryMessageEncoder<ActionSchema> ENCODER =
      new BinaryMessageEncoder<ActionSchema>(MODEL$, SCHEMA$);

  private static final BinaryMessageDecoder<ActionSchema> DECODER =
      new BinaryMessageDecoder<ActionSchema>(MODEL$, SCHEMA$);

  /**
   * Return the BinaryMessageEncoder instance used by this class.
   * @return the message encoder used by this class
   */
  public static BinaryMessageEncoder<ActionSchema> getEncoder() {
    return ENCODER;
  }

  /**
   * Return the BinaryMessageDecoder instance used by this class.
   * @return the message decoder used by this class
   */
  public static BinaryMessageDecoder<ActionSchema> getDecoder() {
    return DECODER;
  }

  /**
   * Create a new BinaryMessageDecoder instance for this class that uses the specified {@link SchemaStore}.
   * @param resolver a {@link SchemaStore} used to find schemas by fingerprint
   * @return a BinaryMessageDecoder instance for this class backed by the given SchemaStore
   */
  public static BinaryMessageDecoder<ActionSchema> createDecoder(SchemaStore resolver) {
    return new BinaryMessageDecoder<ActionSchema>(MODEL$, SCHEMA$, resolver);
  }

  /**
   * Serializes this ActionSchema to a ByteBuffer.
   * @return a buffer holding the serialized data for this instance
   * @throws java.io.IOException if this instance could not be serialized
   */
  public java.nio.ByteBuffer toByteBuffer() throws java.io.IOException {
    return ENCODER.encode(this);
  }

  /**
   * Deserializes a ActionSchema from a ByteBuffer.
   * @param b a byte buffer holding serialized data for an instance of this class
   * @return a ActionSchema instance decoded from the given buffer
   * @throws java.io.IOException if the given bytes could not be deserialized into an instance of this class
   */
  public static ActionSchema fromByteBuffer(
      java.nio.ByteBuffer b) throws java.io.IOException {
    return DECODER.decode(b);
  }

  @Deprecated public java.lang.String id;
  @Deprecated public java.lang.String name;
  @Deprecated public java.lang.String description;
  @Deprecated public java.lang.String subject;
  @Deprecated public java.lang.String resource;
  @Deprecated public java.lang.String createdDate;
  @Deprecated public java.lang.String createdBy;

  /**
   * Default constructor.  Note that this does not initialize fields
   * to their default values from the schema.  If that is desired then
   * one should use <code>newBuilder()</code>.
   */
  public ActionSchema() {}

  /**
   * All-args constructor.
   * @param id The new value for id
   * @param name The new value for name
   * @param description The new value for description
   * @param subject The new value for subject
   * @param resource The new value for resource
   * @param createdDate The new value for createdDate
   * @param createdBy The new value for createdBy
   */
  public ActionSchema(java.lang.String id, java.lang.String name, java.lang.String description, java.lang.String subject, java.lang.String resource, java.lang.String createdDate, java.lang.String createdBy) {
    this.id = id;
    this.name = name;
    this.description = description;
    this.subject = subject;
    this.resource = resource;
    this.createdDate = createdDate;
    this.createdBy = createdBy;
  }

  public org.apache.avro.specific.SpecificData getSpecificData() { return MODEL$; }
  public org.apache.avro.Schema getSchema() { return SCHEMA$; }
  // Used by DatumWriter.  Applications should not call.
  public java.lang.Object get(int field$) {
    switch (field$) {
    case 0: return id;
    case 1: return name;
    case 2: return description;
    case 3: return subject;
    case 4: return resource;
    case 5: return createdDate;
    case 6: return createdBy;
    default: throw new IndexOutOfBoundsException("Invalid index: " + field$);
    }
  }

  // Used by DatumReader.  Applications should not call.
  @SuppressWarnings(value="unchecked")
  public void put(int field$, java.lang.Object value$) {
    switch (field$) {
    case 0: id = value$ != null ? value$.toString() : null; break;
    case 1: name = value$ != null ? value$.toString() : null; break;
    case 2: description = value$ != null ? value$.toString() : null; break;
    case 3: subject = value$ != null ? value$.toString() : null; break;
    case 4: resource = value$ != null ? value$.toString() : null; break;
    case 5: createdDate = value$ != null ? value$.toString() : null; break;
    case 6: createdBy = value$ != null ? value$.toString() : null; break;
    default: throw new IndexOutOfBoundsException("Invalid index: " + field$);
    }
  }

  /**
   * Gets the value of the 'id' field.
   * @return The value of the 'id' field.
   */
  public java.lang.String getId() {
    return id;
  }


  /**
   * Sets the value of the 'id' field.
   * @param value the value to set.
   */
  public void setId(java.lang.String value) {
    this.id = value;
  }

  /**
   * Gets the value of the 'name' field.
   * @return The value of the 'name' field.
   */
  public java.lang.String getName() {
    return name;
  }


  /**
   * Sets the value of the 'name' field.
   * @param value the value to set.
   */
  public void setName(java.lang.String value) {
    this.name = value;
  }

  /**
   * Gets the value of the 'description' field.
   * @return The value of the 'description' field.
   */
  public java.lang.String getDescription() {
    return description;
  }


  /**
   * Sets the value of the 'description' field.
   * @param value the value to set.
   */
  public void setDescription(java.lang.String value) {
    this.description = value;
  }

  /**
   * Gets the value of the 'subject' field.
   * @return The value of the 'subject' field.
   */
  public java.lang.String getSubject() {
    return subject;
  }


  /**
   * Sets the value of the 'subject' field.
   * @param value the value to set.
   */
  public void setSubject(java.lang.String value) {
    this.subject = value;
  }

  /**
   * Gets the value of the 'resource' field.
   * @return The value of the 'resource' field.
   */
  public java.lang.String getResource() {
    return resource;
  }


  /**
   * Sets the value of the 'resource' field.
   * @param value the value to set.
   */
  public void setResource(java.lang.String value) {
    this.resource = value;
  }

  /**
   * Gets the value of the 'createdDate' field.
   * @return The value of the 'createdDate' field.
   */
  public java.lang.String getCreatedDate() {
    return createdDate;
  }


  /**
   * Sets the value of the 'createdDate' field.
   * @param value the value to set.
   */
  public void setCreatedDate(java.lang.String value) {
    this.createdDate = value;
  }

  /**
   * Gets the value of the 'createdBy' field.
   * @return The value of the 'createdBy' field.
   */
  public java.lang.String getCreatedBy() {
    return createdBy;
  }


  /**
   * Sets the value of the 'createdBy' field.
   * @param value the value to set.
   */
  public void setCreatedBy(java.lang.String value) {
    this.createdBy = value;
  }

  /**
   * Creates a new ActionSchema RecordBuilder.
   * @return A new ActionSchema RecordBuilder
   */
  public static com.hsone.finest.feescheduler.avro.action.ActionSchema.Builder newBuilder() {
    return new com.hsone.finest.feescheduler.avro.action.ActionSchema.Builder();
  }

  /**
   * Creates a new ActionSchema RecordBuilder by copying an existing Builder.
   * @param other The existing builder to copy.
   * @return A new ActionSchema RecordBuilder
   */
  public static com.hsone.finest.feescheduler.avro.action.ActionSchema.Builder newBuilder(com.hsone.finest.feescheduler.avro.action.ActionSchema.Builder other) {
    if (other == null) {
      return new com.hsone.finest.feescheduler.avro.action.ActionSchema.Builder();
    } else {
      return new com.hsone.finest.feescheduler.avro.action.ActionSchema.Builder(other);
    }
  }

  /**
   * Creates a new ActionSchema RecordBuilder by copying an existing ActionSchema instance.
   * @param other The existing instance to copy.
   * @return A new ActionSchema RecordBuilder
   */
  public static com.hsone.finest.feescheduler.avro.action.ActionSchema.Builder newBuilder(com.hsone.finest.feescheduler.avro.action.ActionSchema other) {
    if (other == null) {
      return new com.hsone.finest.feescheduler.avro.action.ActionSchema.Builder();
    } else {
      return new com.hsone.finest.feescheduler.avro.action.ActionSchema.Builder(other);
    }
  }

  /**
   * RecordBuilder for ActionSchema instances.
   */
  @org.apache.avro.specific.AvroGenerated
  public static class Builder extends org.apache.avro.specific.SpecificRecordBuilderBase<ActionSchema>
    implements org.apache.avro.data.RecordBuilder<ActionSchema> {

    private java.lang.String id;
    private java.lang.String name;
    private java.lang.String description;
    private java.lang.String subject;
    private java.lang.String resource;
    private java.lang.String createdDate;
    private java.lang.String createdBy;

    /** Creates a new Builder */
    private Builder() {
      super(SCHEMA$);
    }

    /**
     * Creates a Builder by copying an existing Builder.
     * @param other The existing Builder to copy.
     */
    private Builder(com.hsone.finest.feescheduler.avro.action.ActionSchema.Builder other) {
      super(other);
      if (isValidValue(fields()[0], other.id)) {
        this.id = data().deepCopy(fields()[0].schema(), other.id);
        fieldSetFlags()[0] = other.fieldSetFlags()[0];
      }
      if (isValidValue(fields()[1], other.name)) {
        this.name = data().deepCopy(fields()[1].schema(), other.name);
        fieldSetFlags()[1] = other.fieldSetFlags()[1];
      }
      if (isValidValue(fields()[2], other.description)) {
        this.description = data().deepCopy(fields()[2].schema(), other.description);
        fieldSetFlags()[2] = other.fieldSetFlags()[2];
      }
      if (isValidValue(fields()[3], other.subject)) {
        this.subject = data().deepCopy(fields()[3].schema(), other.subject);
        fieldSetFlags()[3] = other.fieldSetFlags()[3];
      }
      if (isValidValue(fields()[4], other.resource)) {
        this.resource = data().deepCopy(fields()[4].schema(), other.resource);
        fieldSetFlags()[4] = other.fieldSetFlags()[4];
      }
      if (isValidValue(fields()[5], other.createdDate)) {
        this.createdDate = data().deepCopy(fields()[5].schema(), other.createdDate);
        fieldSetFlags()[5] = other.fieldSetFlags()[5];
      }
      if (isValidValue(fields()[6], other.createdBy)) {
        this.createdBy = data().deepCopy(fields()[6].schema(), other.createdBy);
        fieldSetFlags()[6] = other.fieldSetFlags()[6];
      }
    }

    /**
     * Creates a Builder by copying an existing ActionSchema instance
     * @param other The existing instance to copy.
     */
    private Builder(com.hsone.finest.feescheduler.avro.action.ActionSchema other) {
      super(SCHEMA$);
      if (isValidValue(fields()[0], other.id)) {
        this.id = data().deepCopy(fields()[0].schema(), other.id);
        fieldSetFlags()[0] = true;
      }
      if (isValidValue(fields()[1], other.name)) {
        this.name = data().deepCopy(fields()[1].schema(), other.name);
        fieldSetFlags()[1] = true;
      }
      if (isValidValue(fields()[2], other.description)) {
        this.description = data().deepCopy(fields()[2].schema(), other.description);
        fieldSetFlags()[2] = true;
      }
      if (isValidValue(fields()[3], other.subject)) {
        this.subject = data().deepCopy(fields()[3].schema(), other.subject);
        fieldSetFlags()[3] = true;
      }
      if (isValidValue(fields()[4], other.resource)) {
        this.resource = data().deepCopy(fields()[4].schema(), other.resource);
        fieldSetFlags()[4] = true;
      }
      if (isValidValue(fields()[5], other.createdDate)) {
        this.createdDate = data().deepCopy(fields()[5].schema(), other.createdDate);
        fieldSetFlags()[5] = true;
      }
      if (isValidValue(fields()[6], other.createdBy)) {
        this.createdBy = data().deepCopy(fields()[6].schema(), other.createdBy);
        fieldSetFlags()[6] = true;
      }
    }

    /**
      * Gets the value of the 'id' field.
      * @return The value.
      */
    public java.lang.String getId() {
      return id;
    }


    /**
      * Sets the value of the 'id' field.
      * @param value The value of 'id'.
      * @return This builder.
      */
    public com.hsone.finest.feescheduler.avro.action.ActionSchema.Builder setId(java.lang.String value) {
      validate(fields()[0], value);
      this.id = value;
      fieldSetFlags()[0] = true;
      return this;
    }

    /**
      * Checks whether the 'id' field has been set.
      * @return True if the 'id' field has been set, false otherwise.
      */
    public boolean hasId() {
      return fieldSetFlags()[0];
    }


    /**
      * Clears the value of the 'id' field.
      * @return This builder.
      */
    public com.hsone.finest.feescheduler.avro.action.ActionSchema.Builder clearId() {
      id = null;
      fieldSetFlags()[0] = false;
      return this;
    }

    /**
      * Gets the value of the 'name' field.
      * @return The value.
      */
    public java.lang.String getName() {
      return name;
    }


    /**
      * Sets the value of the 'name' field.
      * @param value The value of 'name'.
      * @return This builder.
      */
    public com.hsone.finest.feescheduler.avro.action.ActionSchema.Builder setName(java.lang.String value) {
      validate(fields()[1], value);
      this.name = value;
      fieldSetFlags()[1] = true;
      return this;
    }

    /**
      * Checks whether the 'name' field has been set.
      * @return True if the 'name' field has been set, false otherwise.
      */
    public boolean hasName() {
      return fieldSetFlags()[1];
    }


    /**
      * Clears the value of the 'name' field.
      * @return This builder.
      */
    public com.hsone.finest.feescheduler.avro.action.ActionSchema.Builder clearName() {
      name = null;
      fieldSetFlags()[1] = false;
      return this;
    }

    /**
      * Gets the value of the 'description' field.
      * @return The value.
      */
    public java.lang.String getDescription() {
      return description;
    }


    /**
      * Sets the value of the 'description' field.
      * @param value The value of 'description'.
      * @return This builder.
      */
    public com.hsone.finest.feescheduler.avro.action.ActionSchema.Builder setDescription(java.lang.String value) {
      validate(fields()[2], value);
      this.description = value;
      fieldSetFlags()[2] = true;
      return this;
    }

    /**
      * Checks whether the 'description' field has been set.
      * @return True if the 'description' field has been set, false otherwise.
      */
    public boolean hasDescription() {
      return fieldSetFlags()[2];
    }


    /**
      * Clears the value of the 'description' field.
      * @return This builder.
      */
    public com.hsone.finest.feescheduler.avro.action.ActionSchema.Builder clearDescription() {
      description = null;
      fieldSetFlags()[2] = false;
      return this;
    }

    /**
      * Gets the value of the 'subject' field.
      * @return The value.
      */
    public java.lang.String getSubject() {
      return subject;
    }


    /**
      * Sets the value of the 'subject' field.
      * @param value The value of 'subject'.
      * @return This builder.
      */
    public com.hsone.finest.feescheduler.avro.action.ActionSchema.Builder setSubject(java.lang.String value) {
      validate(fields()[3], value);
      this.subject = value;
      fieldSetFlags()[3] = true;
      return this;
    }

    /**
      * Checks whether the 'subject' field has been set.
      * @return True if the 'subject' field has been set, false otherwise.
      */
    public boolean hasSubject() {
      return fieldSetFlags()[3];
    }


    /**
      * Clears the value of the 'subject' field.
      * @return This builder.
      */
    public com.hsone.finest.feescheduler.avro.action.ActionSchema.Builder clearSubject() {
      subject = null;
      fieldSetFlags()[3] = false;
      return this;
    }

    /**
      * Gets the value of the 'resource' field.
      * @return The value.
      */
    public java.lang.String getResource() {
      return resource;
    }


    /**
      * Sets the value of the 'resource' field.
      * @param value The value of 'resource'.
      * @return This builder.
      */
    public com.hsone.finest.feescheduler.avro.action.ActionSchema.Builder setResource(java.lang.String value) {
      validate(fields()[4], value);
      this.resource = value;
      fieldSetFlags()[4] = true;
      return this;
    }

    /**
      * Checks whether the 'resource' field has been set.
      * @return True if the 'resource' field has been set, false otherwise.
      */
    public boolean hasResource() {
      return fieldSetFlags()[4];
    }


    /**
      * Clears the value of the 'resource' field.
      * @return This builder.
      */
    public com.hsone.finest.feescheduler.avro.action.ActionSchema.Builder clearResource() {
      resource = null;
      fieldSetFlags()[4] = false;
      return this;
    }

    /**
      * Gets the value of the 'createdDate' field.
      * @return The value.
      */
    public java.lang.String getCreatedDate() {
      return createdDate;
    }


    /**
      * Sets the value of the 'createdDate' field.
      * @param value The value of 'createdDate'.
      * @return This builder.
      */
    public com.hsone.finest.feescheduler.avro.action.ActionSchema.Builder setCreatedDate(java.lang.String value) {
      validate(fields()[5], value);
      this.createdDate = value;
      fieldSetFlags()[5] = true;
      return this;
    }

    /**
      * Checks whether the 'createdDate' field has been set.
      * @return True if the 'createdDate' field has been set, false otherwise.
      */
    public boolean hasCreatedDate() {
      return fieldSetFlags()[5];
    }


    /**
      * Clears the value of the 'createdDate' field.
      * @return This builder.
      */
    public com.hsone.finest.feescheduler.avro.action.ActionSchema.Builder clearCreatedDate() {
      createdDate = null;
      fieldSetFlags()[5] = false;
      return this;
    }

    /**
      * Gets the value of the 'createdBy' field.
      * @return The value.
      */
    public java.lang.String getCreatedBy() {
      return createdBy;
    }


    /**
      * Sets the value of the 'createdBy' field.
      * @param value The value of 'createdBy'.
      * @return This builder.
      */
    public com.hsone.finest.feescheduler.avro.action.ActionSchema.Builder setCreatedBy(java.lang.String value) {
      validate(fields()[6], value);
      this.createdBy = value;
      fieldSetFlags()[6] = true;
      return this;
    }

    /**
      * Checks whether the 'createdBy' field has been set.
      * @return True if the 'createdBy' field has been set, false otherwise.
      */
    public boolean hasCreatedBy() {
      return fieldSetFlags()[6];
    }


    /**
      * Clears the value of the 'createdBy' field.
      * @return This builder.
      */
    public com.hsone.finest.feescheduler.avro.action.ActionSchema.Builder clearCreatedBy() {
      createdBy = null;
      fieldSetFlags()[6] = false;
      return this;
    }

    @Override
    @SuppressWarnings("unchecked")
    public ActionSchema build() {
      try {
        ActionSchema record = new ActionSchema();
        record.id = fieldSetFlags()[0] ? this.id : (java.lang.String) defaultValue(fields()[0]);
        record.name = fieldSetFlags()[1] ? this.name : (java.lang.String) defaultValue(fields()[1]);
        record.description = fieldSetFlags()[2] ? this.description : (java.lang.String) defaultValue(fields()[2]);
        record.subject = fieldSetFlags()[3] ? this.subject : (java.lang.String) defaultValue(fields()[3]);
        record.resource = fieldSetFlags()[4] ? this.resource : (java.lang.String) defaultValue(fields()[4]);
        record.createdDate = fieldSetFlags()[5] ? this.createdDate : (java.lang.String) defaultValue(fields()[5]);
        record.createdBy = fieldSetFlags()[6] ? this.createdBy : (java.lang.String) defaultValue(fields()[6]);
        return record;
      } catch (org.apache.avro.AvroMissingFieldException e) {
        throw e;
      } catch (java.lang.Exception e) {
        throw new org.apache.avro.AvroRuntimeException(e);
      }
    }
  }

  @SuppressWarnings("unchecked")
  private static final org.apache.avro.io.DatumWriter<ActionSchema>
    WRITER$ = (org.apache.avro.io.DatumWriter<ActionSchema>)MODEL$.createDatumWriter(SCHEMA$);

  @Override public void writeExternal(java.io.ObjectOutput out)
    throws java.io.IOException {
    WRITER$.write(this, SpecificData.getEncoder(out));
  }

  @SuppressWarnings("unchecked")
  private static final org.apache.avro.io.DatumReader<ActionSchema>
    READER$ = (org.apache.avro.io.DatumReader<ActionSchema>)MODEL$.createDatumReader(SCHEMA$);

  @Override public void readExternal(java.io.ObjectInput in)
    throws java.io.IOException {
    READER$.read(this, SpecificData.getDecoder(in));
  }

  @Override protected boolean hasCustomCoders() { return true; }

  @Override public void customEncode(org.apache.avro.io.Encoder out)
    throws java.io.IOException
  {
    out.writeString(this.id);

    out.writeString(this.name);

    out.writeString(this.description);

    out.writeString(this.subject);

    out.writeString(this.resource);

    out.writeString(this.createdDate);

    out.writeString(this.createdBy);

  }

  @Override public void customDecode(org.apache.avro.io.ResolvingDecoder in)
    throws java.io.IOException
  {
    org.apache.avro.Schema.Field[] fieldOrder = in.readFieldOrderIfDiff();
    if (fieldOrder == null) {
      this.id = in.readString();

      this.name = in.readString();

      this.description = in.readString();

      this.subject = in.readString();

      this.resource = in.readString();

      this.createdDate = in.readString();

      this.createdBy = in.readString();

    } else {
      for (int i = 0; i < 7; i++) {
        switch (fieldOrder[i].pos()) {
        case 0:
          this.id = in.readString();
          break;

        case 1:
          this.name = in.readString();
          break;

        case 2:
          this.description = in.readString();
          break;

        case 3:
          this.subject = in.readString();
          break;

        case 4:
          this.resource = in.readString();
          break;

        case 5:
          this.createdDate = in.readString();
          break;

        case 6:
          this.createdBy = in.readString();
          break;

        default:
          throw new java.io.IOException("Corrupt ResolvingDecoder.");
        }
      }
    }
  }
}










